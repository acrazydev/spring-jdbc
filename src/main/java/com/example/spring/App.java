package com.example.spring;

import org.springframework.context.ApplicationContext; 
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		ApplicationContext context = new FileSystemXmlApplicationContext(
				"src/main/java/com/example/spring/config.xml");

		DBOperation dbOperation = (DBOperation) context.getBean("studentJdbcTemplate");

		
		Student st1 = new Student();
		st1.setSid(1);
		st1.setName("Student1");
		st1.setAdd("Place1");
		
		System.out.println(dbOperation.getStudentsDeatils());
		System.out.println(dbOperation.deleteDetails(25));
		System.out.println(dbOperation.getStudentsDeatils());
		((AbstractApplicationContext) context).registerShutdownHook();
	}
}
